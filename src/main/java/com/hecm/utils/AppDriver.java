package com.hecm.utils;

import java.io.IOException;
import java.util.Properties;
import java.util.logging.Logger;

import org.openqa.selenium.Capabilities;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.safari.SafariDriver;

public class AppDriver {

	private static final Logger LOGGER = Logger.getLogger(AppDriver.class.getName());
	private static final String BROWSER_KEY = "Browser";
	private static final String PLATFORM_KEY = "platform";
	private static final String DEFAULT = "default";
	private static WebDriver driver;

	/*
	 * Load the specific browser driver using system property defined in pom.xml
	 */
	public static WebDriver getDriver() {
		Browser browser;
		Platform platform = null;

		if (driver == null) {
			if (System.getProperty(BROWSER_KEY) == null || System.getProperty(BROWSER_KEY).equals(DEFAULT)) {
				browser = Browser.FIREFOX;
			} else {
				browser = Browser.getBrowserType(System.getProperty(BROWSER_KEY));

				if (System.getProperty(PLATFORM_KEY) == null || System.getProperty(PLATFORM_KEY).equals(DEFAULT)) {
					platform = Platform.WIN;
				} else {
					platform = Platform.getPlatformType(System.getProperty(PLATFORM_KEY));
				}
			}

			switch (browser) {
			case CHROME:
				driver = getChromeDriver(platform);
				break;
			case SAFARI:
				driver = getSafariDriver();
				break;
			case IE:
				driver = getIEDriver(platform);
				break;
			case FIREFOX:
			default:
				driver = getFirefoxDriver();
				break;
			}
			doAllBrowserSetup(driver);
		}
		return driver;
	}

	/*
	 * Close the current browser driver and clear the browser session
	 */
	public static void closeDriver() {
		getDriver().quit();
		driver = null;
		AppVariables.APP_DRIVER=null;
		LOGGER.info("Closing the current browser driver");
	}

	/*
	 * Launch the url for environment
	 */

	public static void launchUrl(String path) {
		String market = System.getProperty(AppVariables.APP_MARKET_KEY);

		AppVariables.APP_ENV = System.getProperty(AppVariables.APP_ENV_KEY);
		AppVariables.APP_URL = getURL(AppVariables.APP_ENV + ".environment.url") + market;

		LOGGER.info("Loading the Url: " + AppVariables.APP_URL + path);
		getDriver().get(AppVariables.APP_URL + path);

	}

	public static void launchBackendUrl(String path) {

		String market = System.getProperty(AppVariables.APP_MARKET_KEY);

		AppVariables.APP_ENV = System.getProperty(AppVariables.APP_ENV_KEY);
		AppVariables.APP_URL = getURL(AppVariables.APP_ENV + ".environment.url") + market;
		AppVariables.APP_BACKEND_URL = getURL(AppVariables.APP_ENV + ".backend.environment.url") + market;

		LOGGER.info("Loading the Url: " + AppVariables.APP_BACKEND_URL);
		getDriver().get(AppVariables.APP_BACKEND_URL + path);
	}

	/*
	 * Launch the url for environment
	 */
	public static void launchSecondaryUrl(String urlKey, String path) {
		String market = System.getProperty(AppVariables.APP_MARKET_KEY);

		AppVariables.APP_ENV = System.getProperty(AppVariables.APP_ENV_KEY);
		AppVariables.APP_SECONDARY_URL = getURL(AppVariables.APP_ENV + ".environment" + urlKey + ".url") + market;

		LOGGER.info("Loading the Url: " + AppVariables.APP_URL + path);
		getDriver().get(AppVariables.APP_URL + path);

	}

	private static WebDriver getFirefoxDriver() {
		LOGGER.info("Loading the Firefox driver");
		DesiredCapabilities ieCapabilities = DesiredCapabilities.firefox();
		ieCapabilities.setCapability(FirefoxDriver.PROFILE, true);
		return new FirefoxDriver(ieCapabilities);
	}

	private static WebDriver getChromeDriver(Platform platform) {
		LOGGER.info("Loading the Chrome driver");
		String driverPath = null;

		if (platform.equals(Platform.MAC)) {
			driverPath = getDriverProperty("chromeForMac");
		} else if (platform.equals(Platform.LINUX)) {
			driverPath = getDriverProperty("chromeForLinux");
		} else {
			driverPath = getDriverProperty("chromeForWindows");
		}

		System.setProperty("webdriver.chrome.driver", driverPath);
		ChromeOptions options = new ChromeOptions();
		DesiredCapabilities ieCapabilities = DesiredCapabilities.chrome();
		ieCapabilities.setCapability(ChromeOptions.CAPABILITY, options);
		return new ChromeDriver(ieCapabilities);
	}

	private static WebDriver getIEDriver(Platform platform) {
		LOGGER.info("Loading the IE driver");
		String driverPath = null;

		if (platform.equals(Platform.WIN32)) {
			driverPath = getDriverProperty("ieForWin32");
		} else {
			driverPath = getDriverProperty("ieForWin64");
		}

		System.setProperty("webdriver.ie.driver", driverPath);
		DesiredCapabilities ieCapabilities = DesiredCapabilities.internetExplorer();
		ieCapabilities.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);
		return new InternetExplorerDriver(ieCapabilities);
	}

	private static WebDriver getSafariDriver() {
		LOGGER.info("Loading the Safari driver");
		return new SafariDriver();
	}

	private static void doAllBrowserSetup(WebDriver driver) {
		driver.manage().deleteAllCookies();
		driver.manage().window().maximize();
	}

	public static String getURL(String propKey) {
		PropertyReader propReader = new PropertyReader("/com/hecm/properties/url.properties");
		return propReader.readProperty(propKey);
	}

	public static String getBrowserName() {
		Capabilities cap = ((RemoteWebDriver) driver).getCapabilities();
		String browsername = cap.getBrowserName();
		return browsername;
	}

	public static String getDriverProperty(String propKey) {
		Properties prop = new Properties();
		String propVal = null;

		try {
			prop.load(AppDriver.class.getResourceAsStream("/appDriver.properties"));
			propVal = prop.getProperty(propKey);
		} catch (IOException ie) {
			LOGGER.info(ie.getMessage());
		}

		return propVal;
	}

	/*
	 * Browser type constants
	 */
	public enum Browser {

		FIREFOX, CHROME, IE, SAFARI;

		public static Browser getBrowserType(String browser) throws IllegalArgumentException {
			for (Browser b : values()) {
				if (b.toString().equalsIgnoreCase(browser)) {
					return b;
				}
			}
			throw browserNotFound(browser);
		}

		private static IllegalArgumentException browserNotFound(String browserName) {
			return new IllegalArgumentException(("Invalid browser [" + browserName + "]"));
		}
	}

	public enum Platform {

		WIN, WIN32, WIN64, MAC, LINUX;

		public static Platform getPlatformType(String platform) throws IllegalArgumentException {
			for (Platform p : values()) {
				if (p.toString().equalsIgnoreCase(platform)) {
					return p;
				}
			}
			throw invalidPlatform(platform);
		}

		private static IllegalArgumentException invalidPlatform(String platform) {
			return new IllegalArgumentException(("Invalid platform [" + platform + "]"));
		}
	}

}