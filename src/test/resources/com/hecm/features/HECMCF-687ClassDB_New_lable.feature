@SmokeTest
Feature: Verify the new dropdown lable ClassDB for an admin, when he tries to add/edit college  

Background:
Given I login as an "Admin" user
When I login into HECM

Scenario: Verify the ClassDB dropdown values when adding a new college
And I click on colleges and courses under admin tab
When I click on the Add new college
Then I should be able to see the Class DB dropdown with values  UKP, NAA, ICP

Scenario: Verify the ClassDB dropdown values while editing a new college
And I click on colleges and courses under admin tab
When I click on edit new college
Then I should be able to see the Class DB dropdown with values  UKP, NAA, ICP