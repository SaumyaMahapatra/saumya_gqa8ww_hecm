@SmokeTest
Feature: Verify degree gets created and edited succesfully

Scenario Outline: Verify the fields in reason for Update field in degree edit page for an admin user
Given I login as "<Usertype>" user
When I login into HECM
And I click on the Add new Degree
And I enter the mandatory details in degree page
And I click on save button
Then I should see the degree list
When I click on edit degree
Then I should see the reasons for update
When I choose to select reason for update
And I fill the reason for change
And I click on save button
Then I should see the "Degree list" page

Examples:

|Usertype |
|Admin    |


Scenario Outline: Verify the fields in reason for Update field in degree edit page for College editor user
Given I login as "<Usertype>" user
When I login into HECM
When I click on edit degree
Then I should see the reasons for update
When I choose to select reason for update
And I fill the reason for change
And I click on save button
Then I should see the "Degree list" page 

Examples:

|Usertype     |
|CollegeEditor|
