package com.hecm.testrunner;

import org.junit.runner.RunWith;

import com.hecm.utils.AppDriver;
import com.hecm.utils.AppVariables;

import cucumber.api.CucumberOptions;

@RunWith(ExtendedCucumberRunner.class)
@CucumberOptions(features = "classpath:com/hecm/features/", 
                 plugin = { "pretty", "html:target/cucumber-reports/html","json:target/cucumber.json", "rerun:target/rerun.txt" },
                 glue = "com.hecm.stepdefinitions", 
                 tags = {"@Runme"}
                )




public class Default_TestRunner {
	@BeforeTestSuite
	public static void setup() {
		System.setProperty(AppVariables.APP_MARKET_KEY, AppVariables.APP_MARKET_DEFAULT);
	}

	// @AfterTestSuite
	public static void exitSuite() {
		if (AppVariables.APP_DRIVER != null) {
			AppVariables.APP_DRIVER = null;
			AppDriver.closeDriver();
		}
	}

}