package com.hecm.stepdefinitions;

import com.hecm.pageobjects.CreateDegreePage;
import com.hecm.pageobjects.DegreeListPage;
import com.hecm.pageobjects.HomePage;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class DegreeTestSteps {

	private HomePage adminhomepage = new HomePage();
	private CreateDegreePage admincreatedegree = new CreateDegreePage();
	private DegreeListPage degreelist=new DegreeListPage();

	@And("^I click on the Add new Degree$")
	public void I_click_on_the_add_New_Degree() throws InterruptedException {
		adminhomepage.addNewDegreeButton();
	}

	@And("^I enter the mandatory details in degree page$")
	public void I_enter_mandatory_detail_on_Degree_Page() {
		admincreatedegree.fillDegreeDetails();
	}

	@And("^I click on save button$")
	public void I_click_on_save_button() {
		admincreatedegree.saveDegree();
	}
	@Then("^I should see the degree list$")
	public void I_should_see_the_degree_list() throws InterruptedException
	{
		degreelist.verifydegreeList();
	}
	@When("^I click on edit degree$")
	
	public void I_click_on_edit_degree()
	{
		degreelist.editDegree();
	}
	
	@And("^I should see the reasons for update$")
	
	public void I_should_see_the_reason_for_update()
	{
		admincreatedegree.verifyReasonForUpdate();
		
	}
	@When("^I choose to select reason for update$")
	
	public void I_choose_to_select_reason_for_update()
	{
		admincreatedegree.setReasonforUpdate();
	}
	
	@And("^I fill the reason for change$")
	
	public void I_fill_the_reason_for_change()
	{
		admincreatedegree.commentsForChangeinDegree();
	}
	
	@Then("^I should see the \"(.*?)\" page$")
	
	public void I_should_see_degree_home_page(String degreehomepagetitle)
	{
		degreelist.degreeHomePage(degreehomepagetitle);
	}
	
		
}
