package com.hecm.pageobjects;

import org.openqa.selenium.support.ui.Select;

import com.hecm.utils.AppDriver;
import com.hecm.utils.AppVariables;
import com.hecm.utils.Utils;

import junit.framework.Assert;

public class AdminAreaHomePage {
	
	//adding new college
	
		public void addCollege() {
			Utils.getWebElement(AppVariables.APP_DRIVER, AppVariables.OBJECT_REPO.getLocator("adminareahomepage.add_college.link")).click();
		}
		
	//editing existing college
		
		public void editCollege() {
			
			Utils.getWebElement(AppVariables.APP_DRIVER, AppVariables.OBJECT_REPO.getLocator("adminareahomepage.edit_college.link")).click();
		}
		
	//verifying the ClassDB drop down values
		
		public void verifyClassDbDropdown() {
			
			String[] classDropdownExpected={"UKP","NAA","ICP"};
			
			Select classdbdropdown=Utils.getSelectElement(AppVariables.APP_DRIVER, AppVariables.OBJECT_REPO.getLocator("adminareahomepage.classDB.dropdown"));
			boolean match = true;
			for(int i=0;i<classDropdownExpected.length;i++)
			{
			
				classdbdropdown.selectByIndex(i);
				if(!classdbdropdown.getOptions().get(i).getText().equals(classDropdownExpected[i]))
				{
				match=false;
				break;
				}
				
			}
			Assert.assertTrue("classdbdropdown not match",match);
			
			
		}

}
